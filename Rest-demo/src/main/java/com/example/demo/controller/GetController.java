package com.example.demo.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;

/**
 * @author amirs
 *This controller is ued for GET
 */
@RestController

public class GetController {

	@RequestMapping(value = "/first")
	public String hello() {
		return "Pitambar program !!!!";
	}

	@RequestMapping(value = "/second/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public User helloUser(@PathVariable String userId) {
		User user = new User();
		user.setUserId(userId);
		user.setPassword("Chandan");
		return user;
	}

	@RequestMapping(value = "/third/{userId}/{password}", produces = MediaType.APPLICATION_JSON_VALUE)
	public User thirdUser(@PathVariable String userId, @PathVariable String password) {
		User user = new User();
		user.setUserId(userId);
		user.setPassword(password);
		return user;
	}

	@RequestMapping(value = "/fourth", produces = MediaType.APPLICATION_JSON_VALUE)
	public User fourthUser(@RequestParam String userId, @RequestParam String password) {
		User user = new User();
		user.setUserId(userId);
		user.setPassword(password);
		return user;
	}
}
